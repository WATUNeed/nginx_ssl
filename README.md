# Инструкция по установки SSL сертификата на домен

1. Копируем из репозитория файлы `./docker/nginx/default.conf`
```
server {
  listen 80;

  server_tokens off;

  location /.well-known/acme-challenge/ {
    root /var/www/certbot;
  }

  location / {
    return 200;
  }
}
```

2. Добавляем в свой проект nginx:
```
nginx:
  image: nginx:latest
  ports:
    - 80:80
    - 443:443
  volumes:
    - ./docker/nginx/default.conf:/etc/nginx/conf.d/default.conf:ro
    - ./certbot/www:/var/www/certbot/:ro
    - ./certbot/conf/:/etc/nginx/ssl/:ro

certbot:
  image: certbot/certbot:latest
  volumes:
    - ./certbot/www/:/var/www/certbot/:rw
    - ./certbot/conf/:/etc/letsencrypt/:rw
```

3. Запускаем проект вместе с Nginx:
```
docker compose up -d --build
```

4. Генерируем сертификат:
```
docker compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d domain.ru
```

5. Подменяем содержимое файла `./docker/nginx/default.conf` на значение из `./docker/nginx/prod.conf`. Не забывая подменить домен на нужный.
```
server {
  listen 80;
  listen [::]:80;

  server_name domain.ru;
  server_tokens off;

  location /.well-known/acme-challenge/ {
      root /var/www/certbot;
  }

  location / {
      return 301 https://domain.ru$request_uri;
  }
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;
  http2 on;

  server_name domain.ru;

  ssl_certificate /etc/nginx/ssl/live/domain.ru/fullchain.pem;
  ssl_certificate_key /etc/nginx/ssl/live/domain.ru/privkey.pem;

  location /api {
    proxy_pass http://backend;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Host $server_name;
    proxy_set_header X-Forwarded-Protocol $scheme;
    proxy_redirect off;
  }
}
```

6. Удаляем certbot из docker-compose.yml пока не потребуется.


# Обновление сертификата

1. Добавляем certbot в docker-compose.yml:
```
certbot:
  image: certbot/certbot:latest
  volumes:
    - ./certbot/www/:/var/www/certbot/:rw
    - ./certbot/conf/:/etc/letsencrypt/:rw
```

2. Обновляем сертификат:
```
docker compose run --rm certbot renew --cert-name example.com --force-renewal
```

3. Удаляем certbot из docker-compose.yml пока не потребуется

